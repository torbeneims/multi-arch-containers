{
  description = "My Container Image";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
  };
  outputs = { self, nixpkgs }:
    let
      pkgs = import nixpkgs {
        system = "x86_64-linux";
      };
    in
    {
      multi-arch-containers = pkgs.dockerTools.buildLayeredImage {
        name = "multi-arch-containers";
	tag = "latest";
	contents = with pkgs; [ yarn git ];
          config = {
            Cmd = [ "yarn" "start" ];
            WorkingDir = "/app";
            Copy = [
              { src = ./package.json; dest = "."; }
              { src = ./.; dest = "."; }
            ];
          };
      };
    };
}

